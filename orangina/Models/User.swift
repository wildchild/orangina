//
//  User.swift
//  orangina
//
//  Created by Derouich on 6/14/19.
//  Copyright © 2019 Derouich. All rights reserved.
//

import Foundation


import Foundation

import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import ObjectMapper

public struct User : Mappable {
    
    public var id : String!
    
    public static var connectedUser : User! = nil
    
    public init?(map: Map) {
        
    }
    
    init(id: String) {
        self.id = id
    }
    
    public mutating func mapping(map: Map) {
        id <- map["id"]
    }
}
